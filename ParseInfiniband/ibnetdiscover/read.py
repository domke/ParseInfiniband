#!/usr/bin/env python3

from sys import exit, argv
from os import path
from re import compile, findall, IGNORECASE
from copy import deepcopy
from hashlib import md5


def _analyze_link(ibnet_re, line, lnode):
    if not ibnet_re or not line:
        exit('ERR: empty parameter')

    width = {1: '1', 2: '4', 4: '8', 8: '12', 16: '2'}
    speed = {1: 'SDR', 2: 'DDR', 4: 'QDR', 8: 'FDR10', 14: 'FDR', 25: 'EDR', 50: 'HDR', 100: 'NDR'}
    vlcap = {1: 'VL0', 2: 'VL0-1', 3: 'VL0-3', 4: 'VL0-7', 5: 'VL0-14'}
    lnode['PN'], pguid1, nodeid2, port2 = _analyze_main_link(ibnet_re, line)
    if pguid1:
        pguid1 = pguid1.strip('()').zfill(16)
        lnode['PortGUID'] = pguid1
    else:
        lnode['PortGUID'] = None
    pguid2, name2, lid1, lid2, linksw = _analyze_add1_link(ibnet_re, line)
    if lid1 and not lnode['LID']:
        lnode['LID'] = lid1
    links, linkw, linkv = _analyze_add2_link(ibnet_re, line)
    if not linksw and (links and linkw):
        linksw = '%sx%s' % (width[linkw], speed[links])
    if linkv:
        linkv = vlcap[linkv]
    rnode = {'NodeID': nodeid2, 'PN': port2, 'PortGUID': pguid2, 'Name': name2,
             'LID': lid2, 'LinkSpeed': linksw, 'LinkVLCap': linkv}

    return rnode


def _analyze_main_link(ibnet_re, line):
    if not ibnet_re or not line:
        exit('ERR: empty parameter')

    m = ibnet_re['link'].match(line)
    if m:
        return (int(m.group(1)), m.group(2), m.group(3), int(m.group(4)))
    else:
        return (None, None, None, None)


def _analyze_add1_link(ibnet_re, line):
    if not ibnet_re or not line:
        exit('ERR: empty parameter')

    pguid2, name2, lid1, lid2, linksw = None, None, None, None, None

    m = ibnet_re['apng'].match(line)
    if m:
        pguid2 = m.group(1)

    m = ibnet_re['anme'].match(line)
    if m:
        name2 = m.group(1)

    # lid encoding in ibnetdiscover is weird, for sw its in the main/node info
    # while hca's lid only shows up in links
    lids = findall(ibnet_re['alid'], line)
    if len(lids) == 1:
        lid2 = int(lids[0])
    elif len(lids) == 2:
        lid1, lid2 = int(lids[0]), int(lids[1])

    m = ibnet_re['alsw'].match(line)
    if m:
        linksw = '%sx%s' % (int(m.group(1)), m.group(2))

    return (pguid2, name2, lid1, lid2, linksw)


def _analyze_add2_link(ibnet_re, line):
    if not ibnet_re or not line:
        exit('ERR: empty parameter')

    speed = {1: 14, 2: 25}
    links, linkw, linkv = None, None, None

    m = ibnet_re['adds'].match(line)
    if m:
        links = int(m.group(1))

    m = ibnet_re['addw'].match(line)
    if m:
        linkw = int(m.group(1))

    m = ibnet_re['adde'].match(line)
    if m:
        links = speed[int(m.group(1))]

    m = ibnet_re['addv'].match(line)
    if m:
        linkv = int(m.group(1))

    return (links, linkw, linkv)


def _update_network(network, sw_list, hca_list, lnode, rnode, req_elements):
    pguid_elem = {'PortGUID'}
    link_elem = {'LinkSpeed', 'LinkVLCap'}

    # generate guid for the node, if the ibnetdiscover file doesnt provide one
    if not lnode['NodeGUID']:
        lnode['NodeGUID'] = '0x%016x' % (
            int(md5(lnode['NodeID'].encode('utf-8')).hexdigest(), 16) % pow(2, 64))

    # update the hca/switch lists
    if 'CA' in lnode['Node'] and lnode['NodeGUID'] not in hca_list:
        hca_list.append(lnode['NodeGUID'])
    elif 'SW' in lnode['Node'] and lnode['NodeGUID'] not in sw_list:
        sw_list.append(lnode['NodeGUID'])

    # update network topology; in this state the rnode's GUID is unknown, so
    # use NodeID instead of NodeGUID temporarily ad do real update later
    if lnode['NodeGUID'] not in network:
        network[lnode['NodeGUID']] = {'PN': {}, 'NodeID': lnode['NodeID']}
        for elem in req_elements.difference(pguid_elem.union(link_elem)):
            network[lnode['NodeGUID']][elem] = lnode[elem]
    # multi-port CAs are bit tricky, since they have multiple PortGUID
    for elem in req_elements.intersection(pguid_elem):
        if 'CA' in lnode['Node'] and elem in network[lnode['NodeGUID']]:
            # change the previous str-based PortGUID into a dict
            if not isinstance(network[lnode['NodeGUID']][elem], dict):
                # in case the input lists the port info twice ignore it
                if lnode['PN'] in network[lnode['NodeGUID']]['PN']:
                    continue
                network[lnode['NodeGUID']][elem] = {
                    list(network[lnode['NodeGUID']]['PN'].keys())[0]:
                    network[lnode['NodeGUID']][elem]}
            network[lnode['NodeGUID']][elem][lnode['PN']] = lnode[elem]
        else:
            network[lnode['NodeGUID']][elem] = lnode[elem]
    # now take care of the rest
    network[lnode['NodeGUID']]['PN'][lnode['PN']] = {
        'NodeGUID': rnode['NodeID'], 'PN': rnode['PN'], 'LID': rnode['LID']}
    # only rnode contains the link infos
    for elem in req_elements.intersection(link_elem):
        network[lnode['NodeGUID']]['PN'][lnode['PN']][elem] = rnode[elem]


def _update_incorrect_and_missing_info(network, req_elements):
    # change all NodeGUID to the real value (prev. NodeID used since NodeGUID
    # usually isnt part of the connection info in the ibnetdiscover output
    # additionally, sanitize the LID information for all nodes
    for rnodeGUID in network:
        rnodeID = network[rnodeGUID]['NodeID']
        for lnode in network:
            for pn in network[lnode]['PN']:
                if network[lnode]['PN'][pn]['NodeGUID'] == rnodeID:
                    network[lnode]['PN'][pn]['NodeGUID'] = rnodeGUID
                    if 'LID' in req_elements and network[lnode]['PN'][
                            pn]['LID'] and not network[rnodeGUID]['LID']:
                        network[rnodeGUID]['LID'] = network[
                            lnode]['PN'][pn]['LID']
    for lnode in network:
        for pn in network[lnode]['PN']:
            del network[lnode]['PN'][pn]['LID']

    # some nodes might not have a valid PortGUID (esp. switches)
    # hence, set to NodeGUID if missing to match ibdiagnet lst file
    if 'PortGUID' in req_elements:
        for nodeGUID in network:
            if not network[nodeGUID]['PortGUID']:
                network[nodeGUID]['PortGUID'] = nodeGUID

    # delete NodeID if not requested by the user
    if 'NodeID' not in req_elements:
        for nodeGUID in network:
            del network[nodeGUID]['NodeID']


def read(ibnet_file, full=False, elements=''):
    """ read

    ibnet_file: <path>/<name> for ibnetdiscover file

    TODO
    """

    # initial checks if inputs are ok
    ibnet_file = path.normpath(ibnet_file)
    if not path.exists(ibnet_file):
        raise IOError("ibnet file not found")

    ibnet_re = {
        'ignore': compile(r'^\s*#|^\s*$|^DR\s+path'),
        'VenID': compile(r'^\s*vendid=0x(\w+)', IGNORECASE),
        'DevID': compile(r'^\s*devid=0x(\w+)', IGNORECASE),
        'SysImgGUID': compile(r'^\s*sysimgguid=0x(\w+)', IGNORECASE),
        'NodeGUID': compile(r'^\s*(switch|ca)guid=0x(\w+)', IGNORECASE),
        'swca': compile(r'^\s*(switch|hca|ca)\s+(\d+)\s+"([\w\+\-\s\(\)\{\}\[\]/:;,.><]+)"', IGNORECASE),
        'link': compile(r'^\s*\[(\d+)\]\s*(\(\w+\))?\s+"([\w\+\-\s\(\)\{\}\[\]/:;,.><]+)"\s*\[(\d+)\]'),
        'apng': compile(r'.*"\[\d+\]\s*\((\w+)\)'),
        'anme': compile(r'.*\s+#\s*"([\w\+\-\s\(\)\{\}\[\]/:;,.><]+)"'),
        'aepn': compile(r'.*\s+(\w+)\s+port\s+(\d+)', IGNORECASE),
        'alid': compile(r'.*\s+lid\s+(\d+)', IGNORECASE),
        'almc': compile(r'.*\s+lmc\s+(\d+)', IGNORECASE),
        'alsw': compile(r'.*\s+(\d+)x([SDQFEHNR10]+)'),
        'adds': compile(r'.*\s+s=(\d+)'),
        'addw': compile(r'.*\s+w=(\d+)'),
        'addv': compile(r'.*\s+v=(\d+)'),
        'adde': compile(r'.*\s+e=(\d+)')}

    hca_list = []
    sw_list = []
    network = {}

    all_elem = {
        'Node',
        'VenID',
        'DevID',
        'SysImgGUID',
        'NodeID',
        'Name',
        'Ports',
        'PortGUID',
        'LID',
        'ESP0',
        'LinkSpeed',
        'LinkVLCap'}

    req_elements = set()
    if len(elements) > 0:
        req = elements.split(',')
        req_elements = {elem.strip()
                        for elem in req if elem.strip() in all_elem}
        for elem in req:
            if elem.strip() not in all_elem:
                print('WRN: requested element %s not availalbe' % elem.strip())
    if full:
        req_elements = all_elem

    # init lnode and rnode
    tmp = {'Node': None, 'NodeID': None, 'Ports': None, 'SysImgGUID': None,
           'NodeGUID': None, 'PortGUID': None, 'VenID': None, 'DevID': None,
           'Name': None, 'LID': None, 'PN': None, 'ESP0': None,
           'LinkSpeed': None, 'LinkVLCap': None}
    lnode = deepcopy(tmp)
    rnode = deepcopy(tmp)

    # parse through the ibnet file
    with open(ibnet_file, 'r') as ibnet_input:
        for line in ibnet_input:
            if ibnet_re['ignore'].match(line):
                continue

            # go thru all the links of the particular node
            if ibnet_re['link'].match(line):
                rnode = _analyze_link(ibnet_re, line, lnode)
                _update_network(
                    network,
                    sw_list,
                    hca_list,
                    lnode,
                    rnode,
                    req_elements)
                # remaining links after first one
                for line in ibnet_input:
                    if ibnet_re['ignore'].match(line):
                        continue
                    if not ibnet_re['link'].match(line):
                        break
                    rnode = _analyze_link(ibnet_re, line, lnode)
                    _update_network(network, sw_list, hca_list,
                                    lnode, rnode, req_elements)
                # reset lnode after dropping out here
                lnode = deepcopy(tmp)

            m = ibnet_re['VenID'].match(line)
            if m:
                lnode['VenID'] = m.group(1).zfill(8)
            m = ibnet_re['DevID'].match(line)
            if m:
                lnode['DevID'] = m.group(1).zfill(4)
            m = ibnet_re['SysImgGUID'].match(line)
            if m:
                lnode['SysImgGUID'] = m.group(1).zfill(16)
            m = ibnet_re['NodeGUID'].match(line)
            if m:
                lnode['NodeGUID'] = m.group(2).zfill(16)

            m = ibnet_re['swca'].match(line)
            if m:
                node1, ports1, nid1 = m.group(1), int(m.group(2)), m.group(3)
                lnode['Ports'], lnode['NodeID'] = ports1, nid1
                if 'switch' in node1.lower():
                    lnode['Node'] = 'SW'
                elif 'router' in node1.lower():
                    lnode['Node'] = 'RO'
                else:
                    lnode['Node'] = 'CA'
                m = ibnet_re['anme'].match(line)
                if m:
                    lnode['Name'] = m.group(1)
                m = ibnet_re['aepn'].match(line)
                if m and 'enhanced' in m.group(1):
                    lnode['ESP0'] = True
                elif 'SW' in lnode['Node'] or 'RO' in lnode['Node']:
                    lnode['ESP0'] = False
                else:
                    lnode['ESP0'] = None
                m = ibnet_re['alid'].match(line)
                if m:
                    lnode['LID'] = int(m.group(1))
                m = ibnet_re['almc'].match(line)
                if m:
                    lnode['LMC'] = int(m.group(1))

    _update_incorrect_and_missing_info(network, req_elements)

    return (network, sw_list, hca_list)


if __name__ == "__main__":
    print(read(argv[1]))
    exit(0)
