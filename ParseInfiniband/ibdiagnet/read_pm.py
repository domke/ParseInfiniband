#!/usr/bin/env python3

from sys import exit, argv
from os import path
from re import compile


def read_pm(pm_file, short=True, full=False, counters='port_xmit_data', elements=''):
    """ read_pm

    pm_file: <path>/<name> for performance counter file
    short: dict with NodeGUID keys, ports with port_xmit_data counter
    full: dict includes all information contained in the pm file
    counters: comma separated list of requested counters to read from file,
              e.g., 'port_xmit_data,vl15_dropped'
    elements: comma separated list of requested informations, e.g., 'LID,Dev'

    options for elements: LID, Dev, Name

    returns: dict
    """

    # initial checks if inputs are ok
    pm_file = path.normpath(pm_file)
    if not path.exists(pm_file):
        raise IOError("pm file not found")

    counter_list = [
        'link_down_counter',
        'link_error_recovery_counter',
        'symbol_error_counter',
        'port_rcv_remote_physical_errors',
        'port_rcv_errors',
        'port_xmit_discard',
        'port_rcv_switch_relay_errors',
        'excessive_buffer_errors',
        'excesive_buffer_errors',
        'local_link_integrity_errors',
        'port_rcv_constraint_errors',
        'port_xmit_constraint_errors',
        'vl15_dropped',
        'port_xmit_data',
        'port_rcv_data',
        'port_xmit_pkts',
        'port_rcv_pkts',
        'port_xmit_wait',
        'port_xmit_data_extended',
        'port_rcv_data_extended',
        'port_xmit_pkts_extended',
        'port_rcv_pkts_extended',
        'port_unicast_xmit_pkts',
        'port_unicast_rcv_pkts',
        'port_multicast_xmit_pkts',
        'port_multicast_rcv_pkts',
        'retransmission_per_sec',
        'max_retransmission_rate',
        'port_local_physical_errors',
        'port_malformed_packet_errors',
        'port_buffer_overrun_errors',
        'port_dlid_mapping_errors',
        'port_inactive_discards',
        'port_neighbor_mtu_discards',
        'port_vl_mapping_errors',
        'port_looping_errors',
        'port_sw_lifetime_limit_discards',
        'port_sw_hoq_lifetime_limit_discards']
    counter_mult_by_4 = [
        'port_xmit_data',
        'port_rcv_data',
        'port_xmit_data_extended',
        'port_rcv_data_extended']

    if len(counters) > 0:
       req = counters.split(',')
       req_counters = [c.strip() for c in req if c.strip() in counter_list]
    if full or 'all' in map(str.lower, req_counters):
       req_counters = counter_list

    all_elem = ['LID', 'Dev', 'Name']
    req_elements = []
    if len(elements) > 0:
        req = elements.split(',')
        req_elements = [elem.strip() for elem in req if elem.strip() in all_elem]
    if full:
        req_elements = all_elem

    # ibdiagnet version
    port_ibd1_re = compile(r'(?:Port=|\w+\/U\d+\/P|\w+\/P)(\d+)\s+lid=(\w+)\s+guid=0x(\w+)\s+dev=(\w+)')

    # ibdiagnet2 version uses different header for the port description line
    port_ibd2_re = compile(
        r'Port=(\d+)\s+Lid=(\w+)\s+GUID=0x(\w+)\s+Device=(\w+)\s+Port\s+'
        r'Name=([\w/]+)')

    # regex to find counter name and corresponding value
    counter_re = compile(r'^\s*(\w+)\s*=\s*0x(\w+)\s*$')

    netw_counters = {}
    counter_values = {}
    node = None

    # parse through the performance counter file
    with open(pm_file, 'r') as pm_input:
        for line in pm_input:

            # check if just another counter for the current port
            if counter_re.match(line):
                m = counter_re.match(line)
                name, value = m.group(1).strip(), int(m.group(2), 16)
                if name not in counter_list:
                    print("WRN: found unknown counter (%s)" % name)
                elif name not in req_counters:
                   continue
                if name in counter_values:
                    raise IOError("counter (%s) preset twice for port" % name)
                if name in counter_mult_by_4:
                    counter_values[name] = 4 * value
                else:
                    counter_values[name] = value
                continue

            # or check if a new port section starts
            if port_ibd1_re.match(line):
                # store the previous port if we had collected counters before
                if node:
                    netw_counters[node['GUID']]['Ports'][node['PN']] = counter_values
                    counter_values = {}
                m = port_ibd1_re.match(line)
                nguid, port = m.group(3).lower(), int(m.group(1))
                lid, device = int(m.group(2), 16), m.group(4)
                port_name = ''
                node = {
                    'GUID': nguid,
                    'PN': port,
                    'LID': lid,
                    'Dev': device,
                    'Name': port_name}
            elif port_ibd2_re.match(line):
                # store the previous port if we had collected counters before
                if node:
                    netw_counters[node['GUID']]['Ports'][node['PN']] = counter_values
                    counter_values = {}
                m = port_ibd2_re.match(line)
                nguid, port = m.group(3).lower(), int(m.group(1))
                lid, device = int(m.group(2), 16), m.group(4)
                port_name = m.group(5)
                node = {
                    'GUID': nguid,
                    'PN': port,
                    'LID': lid,
                    'Dev': device,
                    'Name': port_name}
            else:
                continue

            # create new node/ports group
            if node['GUID'] not in netw_counters:
                netw_counters[node['GUID']] = {'Ports': {}}
                for elem in req_elements:
                    netw_counters[node['GUID']][elem] = node[elem]

        else:
            # store the last counter list if we are through the file
            if node:
                netw_counters[node['GUID']]['Ports'][node['PN']] = counter_values

    return netw_counters

if __name__ == "__main__":
    print(read_pm(argv[1]))
    exit(0)
