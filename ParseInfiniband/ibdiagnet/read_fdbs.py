#!/usr/bin/env python3

from sys import exit, argv
from os import path
from re import compile


def read_fdbs(fdbs_file):
    """ read_fdbs
    fdbs_file: <path>/<name> for fdbs file

    returns: dict(key: switch GUID, item: LFT dict(key: LID, item: port))
    """

    # initial checks if inputs are ok
    fdbs_file = path.normpath(fdbs_file)
    if not path.exists(fdbs_file):
        raise IOError("fdbs file not found")

    sw_re = compile(r'osm_ucast_mgr_dump_ucast_routes:\s+Switch\s+0x(\w+)')
    fdbs_re = compile(r'0x(\w+)\s+:\s+(\d+)\s+')

    network = {}
    nguid = None
    lft = {}

    # parse through the fdbs file
    with open(fdbs_file, 'r') as fdbs_input:
        for line in fdbs_input:
            # check if a new switch section starts
            if sw_re.match(line):
                # store the previous switch if we had collected LFTs before
                if nguid:
                    network[nguid] = lft
                    lft = {}
                nguid = sw_re.match(line).group(1).lower()

            # or if we find valid LFT entried (ignore `UNKOWN` egress ports)
            if fdbs_re.match(line):
                m = fdbs_re.match(line)
                lid, port = int(m.group(1), 16), int(m.group(2))
                lft[lid] = port
        else:
            # store the last LFT if we are through the file
            if nguid:
                network[nguid] = lft

    return network


if __name__ == "__main__":
    print(read_fdbs(argv[1]))
    exit(0)
