#!/usr/bin/env python3

from sys import exit, argv
from os import path
from re import compile


def read_lst(lst_file, full=False, elements=''):
    """ read_lst

    lst_file: <path>/<name> for lst file
    default: only topology with NodeGUID and egress ports returned
    full: dict includes all information contained in the lst file
    elements: comma separated list of requested informations, e.g., 'VenID,LID'

    options for elements: Node, Ports, SystemGUID, PortGUID, VenID, DevID, Rev,
                          Name, LID, LinkSpeed, LinkStatus

    returns: tuple(<dict containing the topology>,
                   <list of switch NodeGUIDs>,
                   <list of CA NodeGUIDs>)

    General rule:
     * guids will be converted to lowercase
     * hex values converted into int, e.g. port numbers or LIDs
     * rest will be same as in input file
    """

    # initial checks if inputs are ok
    lst_file = path.normpath(lst_file)
    if not path.exists(lst_file):
        raise IOError("lst file not found")

    lst_re = compile(r'{\s+([\w\-]+)\s+Ports:(\w+)\s+SystemGUID:(\w+)\s+'
                     + r'NodeGUID:(\w+)\s+PortGUID:(\w+)\s+VenID:(\w+)\s+'
                     + r'DevID:(\w+)\s+Rev:(\w+)\s+{(.+)}\s+LID:(\w+)\s+'
                     + r'PN:(\w+)\s+}\s+'
                     + r'{\s+([\w\-]+)\s+Ports:(\w+)\s+SystemGUID:(\w+)\s+'
                     + r'NodeGUID:(\w+)\s+PortGUID:(\w+)\s+VenID:(\w+)\s+'
                     + r'DevID:(\w+)\s+Rev:(\w+)\s+{(.+)}\s+LID:(\w+)\s+'
                     + r'PN:(\w+)\s+}\s+'
                     + r'PHY=(\d+)x\s+LOG=([A-Z]+)\s+SPD=(\d\.\d|\d+)')

    hca_list = []
    sw_list = []
    network = {}
    # not sure how FDR10 is encoded, so skip it for now
    speed = {'2.5': 'SDR', '5': 'DDR', '10': 'QDR', '14': 'FDR', '25': 'EDR'}

    all_elem = {
        'Node',
        'Ports',
        'SystemGUID',
        'PortGUID',
        'VenID',
        'DevID',
        'Rev',
        'Name',
        'LID',
        'LinkSpeed',
        'LinkStatus'}
    pguid_elem = {'PortGUID'}
    link_elem = {'LinkSpeed', 'LinkStatus'}

    req_elements = set()
    if len(elements) > 0:
        req = elements.split(',')
        req_elements = {elem.strip()
                        for elem in req if elem.strip() in all_elem}
    if full:
        req_elements = all_elem

    # parse through the lst file
    with open(lst_file, 'r') as lst_input:
        for line in lst_input:
            if not lst_re.match(line):
                continue
            else:
                m = lst_re.match(line)

            # get link info
            lwidth, lstat, lspeed = int(m.group(23)), m.group(24), m.group(25)
            lspeed = '%sx%s' % (lwidth, speed[lspeed])

            # get information from left side
            node1, ports1 = m.group(1), int(m.group(2), 16)
            sguid1, nguid1 = m.group(3).lower(), m.group(4).lower()
            pguid1, vid1 = m.group(5).lower(), m.group(6)
            did1, rev1 = m.group(7), m.group(8)
            name1, lid1 = m.group(9), int(m.group(10), 16)
            pn1 = int(m.group(11), 16)
            lnode = {'Node': node1, 'Ports': ports1, 'SystemGUID': sguid1,
                     'NodeGUID': nguid1, 'PortGUID': pguid1, 'VenID': vid1,
                     'DevID': did1, 'Rev': rev1, 'Name': name1, 'LID': lid1,
                     'PN': pn1, 'LinkSpeed': lspeed, 'LinkStatus': lstat}

            # get information from right side
            node2, ports2 = m.group(12), int(m.group(13), 16)
            sguid2, nguid2 = m.group(14).lower(), m.group(15).lower()
            pguid2, vid2 = m.group(16).lower(), m.group(17)
            did2, rev2 = m.group(18), m.group(19)
            name2, lid2 = m.group(20), int(m.group(21), 16)
            pn2 = int(m.group(22), 16)
            rnode = {'Node': node2, 'Ports': ports2, 'SystemGUID': sguid2,
                     'NodeGUID': nguid2, 'PortGUID': pguid2, 'VenID': vid2,
                     'DevID': did2, 'Rev': rev2, 'Name': name2, 'LID': lid2,
                     'PN': pn2, 'LinkSpeed': lspeed, 'LinkStatus': lstat}

            # update the hca/switch lists
            if 'CA' in lnode['Node'] and lnode['NodeGUID'] not in hca_list:
                hca_list.append(lnode['NodeGUID'])
            elif 'SW' in lnode['Node'] and lnode['NodeGUID'] not in sw_list:
                sw_list.append(lnode['NodeGUID'])

            if 'CA' in rnode['Node'] and rnode['NodeGUID'] not in hca_list:
                hca_list.append(rnode['NodeGUID'])
            elif 'SW' in rnode['Node'] and rnode['NodeGUID'] not in sw_list:
                sw_list.append(rnode['NodeGUID'])

            # update network topology for the lNode
            if lnode['NodeGUID'] not in network:
                network[lnode['NodeGUID']] = {'PN': {}}
                for elem in req_elements.difference(pguid_elem.union(link_elem)):
                    network[lnode['NodeGUID']][elem] = lnode[elem]
            # multi-port CAs are bit tricky, since they have multiple PortGUID
            for elem in req_elements.intersection(pguid_elem):
                if 'CA' in lnode['Node'] and elem in network[lnode['NodeGUID']]:
                    # change the previous str-based PortGUID into a dict
                    if not isinstance(network[lnode['NodeGUID']][elem], dict):
                        # in case the input lists the port info twice ignore it
                        if lnode['PN'] in network[lnode['NodeGUID']]['PN']:
                            continue
                        network[lnode['NodeGUID']][elem] = {
                            list(network[lnode['NodeGUID']]['PN'].keys())[0]:
                            network[lnode['NodeGUID']][elem]}
                    network[lnode['NodeGUID']][elem][lnode['PN']] = lnode[elem]
                else:
                    network[lnode['NodeGUID']][elem] = lnode[elem]
            # now take care of the rest
            network[lnode['NodeGUID']]['PN'][lnode['PN']] = {
                'NodeGUID': rnode['NodeGUID'], 'PN': rnode['PN']}
            for elem in req_elements.intersection(link_elem):
                network[lnode['NodeGUID']]['PN'][lnode['PN']][elem] = lnode[elem]

            # update network topology for the rNode
            if rnode['NodeGUID'] not in network:
                network[rnode['NodeGUID']] = {'PN': {}}
                for elem in req_elements.difference(pguid_elem.union(link_elem)):
                    network[rnode['NodeGUID']][elem] = rnode[elem]
            # multi-port CAs are bit tricky, since they have multiple PortGUID
            for elem in req_elements.intersection(pguid_elem):
                if 'CA' in rnode['Node'] and elem in network[rnode['NodeGUID']]:
                    # change the previous str-based PortGUID into a dict
                    if not isinstance(network[rnode['NodeGUID']][elem], dict):
                        if rnode['PN'] == network[rnode['NodeGUID']]['PN']:
                            continue
                        network[rnode['NodeGUID']][elem] = {
                            list(network[rnode['NodeGUID']]['PN'].keys())[0]:
                            network[rnode['NodeGUID']][elem]}
                    network[rnode['NodeGUID']][elem][rnode['PN']] = rnode[elem]
                else:
                    network[rnode['NodeGUID']][elem] = rnode[elem]
            # now take care of the rest
            network[rnode['NodeGUID']]['PN'][rnode['PN']] = {
                'NodeGUID': lnode['NodeGUID'], 'PN': lnode['PN']}
            for elem in req_elements.intersection(link_elem):
                network[rnode['NodeGUID']]['PN'][rnode['PN']][elem] = rnode[elem]

    return (network, sw_list, hca_list)


if __name__ == "__main__":
    print(read_lst(argv[1]))
    exit(0)
