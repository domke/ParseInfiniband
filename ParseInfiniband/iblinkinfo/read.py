#!/usr/bin/env python3

from sys import exit, argv
from os import path
from re import compile


def read(link_file, short=True, full=False, elements=''):
    """ read

    return:
    """

    # initial checks if inputs are ok
    link_file = path.normpath(link_file)
    if not path.exists(link_file):
        raise IOError("iblinkinfo output not found")

    # 1. name
    ca_re = compile(r'\s*CA:\s+(.*)\s*:')
    # 1. guid, 2. lid, 3. portNr
    ca_link_1p_re = compile(r'\s+0x(\w+)\s+(\d+)\s+(\d+)\[.*\]')
    # 1. linkwidth, 2. linkspeed, 3. lid, 4. portNr, 5. name, 6. note
    ca_link_2p_re = compile(
        r'\s+==\(\s+(\d+)X\s+([\d\.]+)\sGbps\s+Active/\s+LinkUp\)==>\s+(\d+)\s+(\d+)\[.*\]\s+"(.+)"\s+\((.*)\)')

    # 1. guid, 2. name
    sw_re = compile(r'\s*Switch:\s+0x(\w+)\s+(.*)\s*:')
    # 1. lid, 2. portNr, 3. linkwidth, 4. linkspeed, 5. lid, 6. portNr,
    # 7. name, 8. note
    sw_link_re = compile(
        r'\s+(\d+)\s+(\d+)\[.*\]\s+==\(\s+(\d+)X\s+([\d\.]+)\sGbps\s+Active/\s+LinkUp\)==>\s+(\d+)\s+(\d+)\[.*\]\s+"(.+)"\s+\((.*)\)')

    freqs = {
        '2.5': 'SDR',
        '5.0': 'DDR',
        '10.0': 'QDR',
        '10.3125': 'FDR10',
        '14.0625': 'FDR',
        '25.78125': 'EDR'}

    sw_list, hca_list = [], []
    network = {}
    nguid = None

    with open(link_file, 'r') as link_input:
        for line in link_input:
            if ca_re.match(line):
                name = ca_re.match(line).group(1)
                try:
                    line = link_input.next()
                except StopIteration:
                    raise IOError(
                        "iblinkinfo output incomplete, line after CA missing")
                # get some CA information from first part
                if ca_link_1p_re.match(line):
                    m = ca_link_1p_re.match(line)
                    # iblinkinfo lists pguids not node guids -> include in doc
                    pguid, llid, lport = m.group(
                        1), int(m.group(2)), int(m.group(3))
                    if pguid in network:
                        raise IOError("same CA found multiple times")
                    network[pguid] = {'PN': {}, 'LID': llid}
                    hca_list.append(pguid)
                    # and now parse the rest of the link information
                    if ca_link_2p_re.match(line):
                        m = ca_link_1p_re.match(line)
                        lw, lf = int(m.group(1)), m.group(2).strip()
                        rlid, rport = int(m.group(3)), int(m.group(4))
                        rname, note = m.group(5), m.group(6).strip()
                        network[pguid]['PN'][lport] = {
                            'LID': rlid,
                            'PN': rport,
                            'Name': rname,
                            'speed': '%sx%s' % (lw, linkspeeds[lf]),
                            'Note': note}
                else:
                    raise IOError(
                        "iblinkinfo output incomplete, line after CA broken")
            elif sw_re.match(line):
                m = sw_re.match(line)
                nguid, name = m.group(1), m.group(2)
                if nguid in network:
                    raise IOError("same switch found multiple times")
                network[nguid] = {'PN': {}, 'Name': name}
                sw_list.append(nguid)
            elif sw_link_re.match(line) and nguid:
                m = sw_link_re.match(line)
                llid, lport = int(m.group(1)), int(m.group(2))
                lw, lf = int(m.group(3)), m.group(4).strip()
                rlid, rport = int(m.group(5)), int(m.group(6))
                rname, note = m.group(7), m.group(8).strip()
                if 'LID' in network[nguid]:
                    if network[nguid]['LID'] == llid:
                        raise IOError("same switch with different LIDs found")
                else:
                    network[nguid]['LID'] = llid
                network[nguid]['PN'][lport] = {
                    'LID': rlid,
                    'PN': rport,
                    'Name': rname,
                    'speed': '%sx%s' % (lw, linkspeeds[lf]),
                    'Note': note}
            else:
                raise IOError(
                    "iblinkinfo has unknown structure, improve parser")

    return (network, sw_list, hca_list)

if __name__ == "__main__":
    print(read(argv[1]))
    exit(0)
