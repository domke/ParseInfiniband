#!/usr/bin/env python

from distutils.core import setup
from setuptools import find_packages

setup(name='ParseInfiniband',
      version='0.1.1',
      author='Jens Domke',
      author_email='jens.domke@tu-dresden.de',
      url='https://gitlab.com/u/domke',
      description='parser for various outputs of InfiniBand tools',
      long_description=open('README.md').read(),
      license='BSD 3-Clause License (New BSD)',
      packages=find_packages(exclude=['']),
      )
