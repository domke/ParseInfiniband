# ParseInfiniband

TODO

## Dependency

(none so far)

## Installation

```bash
git clone https://gitlab.com/domke/ParseInfiniband.git
cd ParseInfiniband
pip install --user .
```

## Usage example

```
#!/usr/bin/env python3

from ParseInfiniband import ibdiagnet, ibnetdiscover

print('Checking for discrepancy between ibdiagnet lst and ibnetdiscover...\n')

ibd_net, ibd_sw, ibd_ca = ibdiagnet.read_lst(
    './ibdiagnet2.lst', False, 'Ports, Name, LID, LinkSpeed')

ibn_net, ibn_sw, ibn_ca = ibnetdiscover.read(
    './ibnetdiscover.log', False, 'Ports, Name, LID, LinkSpeed')

ibd_sw = set(ibd_sw)
ibn_sw = set(ibn_sw)
print('Difference in lists of switch GUIDs:')
for sw in ibd_sw.symmetric_difference(ibn_sw):
    print('  %s' % sw)

ibd_ca = set(ibd_ca)
ibn_ca = set(ibn_ca)
print('Difference in lists of ca GUIDs:')
for ca in ibd_ca.symmetric_difference(ibn_ca):
    print('  %s' % ca)

print('Only in ibdiagnet:')
for node in set(ibd_net.keys()).difference(set(ibn_net.keys())):
    print('  %s: %s' % (node, ibd_net[node]))

print('Only in ibnetdiscover:')
for node in set(ibn_net.keys()).difference(set(ibd_net.keys())):
    print('  %s: %s' % (node, ibn_net[node]))

print('Missmatch between ibdiagnet and ibnetdiscover information:')
for node in set(ibd_net.keys()).intersection(set(ibn_net.keys())):
    if ''.join(sorted('%s' % ibd_net[node])).lower() != ''.join(
            sorted('%s' % ibn_net[node])).lower():
        print('  %s: %s  vs.  %s' % (node, ibd_net[node], ibn_net[node]))
```
